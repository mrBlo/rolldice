package com.example.android.architecture

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

class MyLifecycleObserver : LifecycleObserver {

    //you can put functionality that responds to Lifecycle processes here
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun OnCreateEvent() {
        Log.i(LOG_TAG, "OnCreate")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun OnStartEvent() {
        Log.i(LOG_TAG, "OnStart")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun OnResumeEvent() {
        Log.i(LOG_TAG, "OnResume")
    }

}