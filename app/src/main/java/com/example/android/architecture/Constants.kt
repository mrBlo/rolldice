package com.example.android.architecture

const val LOG_TAG = "roll_the_dice"
//the constants below will be used in saving the state of the activity even when screen
//orientation changes
const val HEADLINE_TEXT = "headline_text"
const val DICE_COLLECTION = "dice_collection"
const val CONFIG_CHANGE = "config_change"