package com.example.android.architecture

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.android.architecture.viewModel.AboutActivity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    //Ref to Drawer Layout
    private val drawerLayout by lazy {
        findViewById<DrawerLayout>(R.id.drawer_layout)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //set content view to load nav drawer layout
        setContentView(R.layout.activity_main_nav)
        //use Toolbar import from androidx.appcombat
        val toolbar = findViewById<Toolbar>(R.id.toolbar_nav)
        setSupportActionBar(toolbar)

        //Button Reference and onClick using Lambda expression
        rollButton.setOnClickListener {
            //moving to next Activity
            val intent = Intent(this, DiceActivity::class.java)
            startActivity(intent)
        }

        //References to Nav variables
        val navView = findViewById<NavigationView>(R.id.nav_view)
        //Register activity to handle the nav selection events
        navView.setNavigationItemSelectedListener(this)

        //toggle button
        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.open_nav_drawer,
            R.string.close_nav_drawer
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        //sync state allows toggle to open and close
        
    }

    //Creating Options Menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //inflate menu file
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_about) {
            //using companion object to start Another activity
            AboutActivity.start(this)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        drawerLayout.closeDrawer(GravityCompat.START)
        //the above LOC ensures that the nav closes after item is selected
        when (item.itemId) {
            R.id.action_about -> //using companion object to start Another activity
                AboutActivity.start(this)
            // R.id.action_settings->
        }
        return true

    }

}
