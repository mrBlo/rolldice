package com.example.android.architecture

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.android.architecture.viewModel.DiceViewModel
import kotlinx.android.synthetic.main.activity_dice.*

class DiceActivity : AppCompatActivity() {

    //View Model var
    private lateinit var viewModel: DiceViewModel


    //Using lazy initiation
    private val imageViews by lazy {
        arrayOf<ImageView>(
            findViewById(R.id.die1),
            findViewById(R.id.die2),
            findViewById(R.id.die3),
            findViewById(R.id.die4),
            findViewById(R.id.die5)
        )
    }
    private val headline by lazy {
        findViewById<TextView>(R.id.headline)
    }
    //or by using lateinit
    //  private lateinit var headline : TextView;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dice)
        setSupportActionBar(toolbar)

        //set back button on toolbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //setting customized icon as back button
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_check)

        //instantiation of view model
        viewModel = ViewModelProviders.of(this).get(DiceViewModel::class.java)
        viewModel.headline.observe(this, Observer { headline.text = it })
        viewModel.dice.observe(this, Observer { updateDisplay(it) })


/*Commented out because the variables have been handled with the View Model Architecture*/
//        //getting saved Instant State variables if they've been set or setting default variables
//        headlinetext= savedInstanceState?.getString(HEADLINE_TEXT) //"?" shows that savedinstancestate var can be null hence....
//            ?: "Welcome to my app" //...set default value
//        dice= savedInstanceState?.getIntArray(DICE_COLLECTION)
//            ?: intArrayOf(6,6,6,6,6)

        //check to see if onSaveInstance is set already
        val CONFIG_CHANGE = savedInstanceState?.getBoolean(CONFIG_CHANGE)
            ?: false
        if (CONFIG_CHANGE.not()) {
            //the "not" method is equivalent to false
            viewModel.rollDice()

        }

        //lifecycle object is from the Activity Superclass which implements lifecycle owner
        lifecycle.addObserver(MyLifecycleObserver())

        //Explicitly findview by id in Kotlin
        // headline= findViewById(R.id.headline)


    }


    private fun updateDisplay(dice: IntArray) {
        for (i in 0 until imageViews.size) {
            //until keyword is equalivalent to imageview.size -1  ie.looping thru indexes
            val diceid = when (dice[i]) {
                1 -> R.drawable.die_1
                2 -> R.drawable.die_2
                3 -> R.drawable.die_3
                4 -> R.drawable.die_4
                5 -> R.drawable.die_5
                6 -> R.drawable.die_6
                else -> R.drawable.die_6
            }
            //set image resources with ids
            imageViews[i].setImageResource(diceid)
        }

    }

/*Commented out because the variables have been handled with the View Model Architecture*/
    //saving Activity state, use system method onSaveInstanceState
//    override fun onSaveInstanceState(outState: Bundle?) {
//        //save bundle variables so that they will avaiable when onCreate is called again
//        //this is helpful for screen orientation change
//        outState?.putString(HEADLINE_TEXT,headlinetext) // "?" because bundle obj is nullable
//        outState?.putIntArray(DICE_COLLECTION,dice)
//
//        super.onSaveInstanceState(outState)
//    }

    //using SavedState to manage UI config changes
    override fun onSaveInstanceState(outState: Bundle?) {
        //use below line of code to note destroying activity due to configuration change
        outState?.putBoolean(CONFIG_CHANGE, true)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_dice, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_share -> shareResult()

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun shareResult(): Boolean {
        val intent = Intent().apply {
            //'apply' method gives you access to set multiple properties of Intent simultaneously
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "I rolled the dice: ${viewModel.headline.value}")
            type = "text/plain"
        }
        startActivity(intent)
        return true
    }

}
